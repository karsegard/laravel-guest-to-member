<?php

namespace KDA\G2M;

use Illuminate\Http\Request;

abstract class Ownable implements Contract\Auth, Contract\Token, Contract\Resource
{


    use Traits\LoadFromCookie;
    use Traits\GuestResource;
    use Traits\CustomerResource;
    use Traits\GuestToMember;

    protected $inits = [];
    protected $guestResourceEnabled = true;
    public function __get($name)
    {
        if ($name == 'cart') {
            $this->init();
        }
        return $this->cart;
    }

    function __construct(Request $request)
    {

        $this->request = $request;
        $this->bootCart();
    }

    public function bootCart()
    {
        //$reflection = new  \ReflectionClass($this);
        $traits = [
            Traits\LoadFromCookie::class,
            Traits\GuestResource::class,
            Traits\CustomerResource::class,
            Traits\GuestToMember::class,

        ];
        foreach ($traits as $name) {
            $name = (new \ReflectionClass($name))->getShortName();
            $init = "initialize" . $name;
            if (method_exists($this, $init)) {
                $this->inits[] = $init;
            }
        }
        $this->init();

    }


    public function init()
    {
        foreach ($this->inits as $init) {
            $this->$init();
        }
    }
}
