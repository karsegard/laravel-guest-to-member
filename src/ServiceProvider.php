<?php

namespace KDA\G2M;

use KDA\Laravel\PackageServiceProvider;

use Illuminate\Http\Request;

class ServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\ProvidesBlueprint;

   
    protected function packageBaseDir () {
        return dirname(__DIR__,1);
    }

    public function postRegister(){
        $this->blueprints =[
            'guestable'=> function($token_field='token',$ownable_morph='owned'){
                $this->string($token_field)->nullable();
                $this->nullableNumericMorphs($ownable_morph);
            }
        ];
    }
   
    public function bootSelf(){

    }
   
}
