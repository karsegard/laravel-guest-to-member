<?php

namespace KDA\G2M\Traits;

use Log;
use Cookie;

trait GuestToMember
{
    public function initializeGuestToMember()
    {
        Log::debug('Began GuestToMember ');

        if ($this->isAuthenticated() === true) {
            if (!$this->getResource()) {
                Log::debug('User is authenticated and has no resource loaded');

                //$cart = $this->getCartFromCookie();
                $resource = $this->getResourceFromCookie();

                if ($resource) {
                    Log::debug('User is authenticated and has a resource in cookie');
                    if (!$resource->owned) {
                        Log::debug('resource is not yet owned');
                        Log::debug('Associating resource to user ', [$resource->id, $this->getUser()->id]);

                        $resource->owned()->associate($this->getUser())->save();
                        $this->resource = $resource;
                        Log::debug('resource associated');
                    } else {
                        Log::debug('resource aleady associated', [$resource->owned->id, $resource->id, $this->getUser()->id]);
                    }
                }
            } else { //merge carts
               
                if ($resource = $this->getResourceFromCookie()) {
                    $this->merge($resource);
                }
            }
        }

        if ($this->isAuthenticated()) {
            Log::debug('Forgetting cookies');

           \Cookie::queue(Cookie::forget($this->getTokenKeyName()));
           \Cookie::queue(Cookie::forget($this->getTokenKeIdName()));
        }
        Log::debug('GuestToMember finished');

        return;
    }
}
