<?php

namespace KDA\G2M\Traits;

use App\Http\Controllers\Controller;
use KDA\Shop\Cart\Models\Cart;
use Log;

trait CustomerResource
{
    public function initializeCustomerResource()
    {
        Log::debug('Began CustomerResource');
        if ($this->isAuthenticated() === true && !$this->hasResource()) {
            Log::debug('User is authenticated and has no resource');

            if ($resource = $this->retrieveMemberResource()) {
//                $this->cart = $cart;
                $this->setResource($resource);
                Log::debug('Retrieved '.$this->getTokenKeIdName().' for user ', [$this->getResource()->id, $this->getUser()->id]);
            } else {
               // $c = Cart::createOwned($this->user);
               // $this->cart = $c;
                $this->setResource($this->createRegistered());
                Log::debug('Created '.$this->getTokenKeIdName().' for user ', [$this->getResource()->id, $this->getUser()->id]);
            }
        }
    }


    //public function retrieveMemberResource()
    //public function getLatestCart()
    /*{
        return Cart::unlocked()
        ->whereHasMorph('owned', get_class($this->user), function ($q) {
            $q->where('id', $this->user->id);
        })
        ->orderBy('id','desc')->first();
    }*/
}
