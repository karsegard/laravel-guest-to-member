<?php

namespace KDA\G2M\Traits;

use Log;
use Cookie;

trait GuestResource
{
    public function initializeGuestResource()
    {
        Log::debug('Began GuestResource');
     //   if ($this->guestResourceEnabled) {
            if (!$this->isAuthenticated() && !$this->hasResource()) {
                Log::debug('User is not authenticated nor has resource');


                Log::debug('Creating resource');

                //$c= Cart::createAnonymous();
                $resource = $this->createAnonymous();
                //$this->cart = $c;
                $this->setResource($resource);
                \Cookie::queue(Cookie::make($this->getTokenKeyName(), $this->getResource()->token, $this->getTokenExpiration()));
                \Cookie::queue(Cookie::make($this->getTokenKeIdName(), $this->getResource()->id, $this->getTokenExpiration()));
                Log::debug('Created resourece ', [$resource->id]);
            }
     //   }
    }
}
