<?php

namespace KDA\G2M\Traits;

use Log;
trait LoadFromCookie
{
    public function initializeLoadFromCookie()
    {
        Log::debug('Began LoadFromCookie');

        if (!$this->isAuthenticated()) {
            $this->setResource($this->getResourceFromCookie());
            
        }
    }

    public function getResourceFromCookie()
    {

        Log::debug('Retrieving resouce from cookie');
        
        $token = $this->request->cookie($this->getTokenKeyName());
        $id = $this->request->cookie($this->getTokenKeIdName());
        Log::debug('',[$token,$id]);

        if (!empty($token) && !empty($id)) {


           // $cart=  Cart::unlocked()->find($id);
            $resource = $this->retrieveGuestResource($id,$token);

            Log::debug('Found cookie with id',[$id]);

            //if ($cart  && ($token == $cart->token)) {
            if($resource){
                return $resource;
            }

            Log::debug('Resource not found',[$token,$id]);

        }

        return NULL;
    }
}
