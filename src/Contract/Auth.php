<?php
namespace KDA\G2M\Contract;


interface Auth{


    public function getUser();
    public function isAuthenticated();

    
}