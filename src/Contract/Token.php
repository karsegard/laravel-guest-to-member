<?php
namespace KDA\G2M\Contract;


interface Token{


    public function getTokenKeyName();
    public function getTokenKeIdName();
    public function getTokenExpiration();
    
}