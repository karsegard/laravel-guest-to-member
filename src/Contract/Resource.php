<?php
namespace KDA\G2M\Contract;


interface Resource{


    public function getResource();
    public function createAnonymous();
    public function createRegistered();

    public function retrieveGuestResource($id,$token);
    public function hasResource();
    public function setResource($resource);

    public function retrieveMemberResource();
}